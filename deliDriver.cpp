//driver program for the deli
//need to still write

#include "Deli.h"
#include "Cart.h"
#include "Scale.h"
#include "Price_list.h"


#include <iostream>
#include <string>

using std::string; 
using std::cout; 
using std::cin; 
using std::endl;

int main() 
{
	int choice; 
	Deli deli;
	Cart *currentCust = NULL;  
	cout<< "Welcome to the Deli Program!" << endl;
	cout<< "----------------------------" << endl; 
	do 
	{
		cout<<"Would you like to happen next: " << endl;
		cout<<"------------------------------"<< endl; 
		cout<<"1. Arrival of customer" << endl; 	
		cout<<"2. Select a customer to serve" << endl; 
		cout<<"3. Check out the current customer" << endl;
		cout<<"4. Set price of an item" << endl; 
		cout<<"5. Print a report of the total revenues for the day"<< endl; 
		cout<<"6. New day"<< endl;
		cout<<"7. Quit"<< endl; 
		cout<<"Please enter choice: "; 
		cin>> choice;

		switch (choice) 
		{
			case 1:
				deli.newCustomer(); 
				break;
			case 2:
				currentCust = deli.selectCustomer(); 
				break;
			case 3:
				if(currentCust == NULL) 
				{
					cout << "There is not a customer currently being served!" << endl;
				}
				else	
				{
					deli.checkOut(currentCust); 
					currentCust = NULL;
				} 
				break; 
			case 4:
				deli.changePrices();
				break; 
			case 5: 
				deli.report();
				break; 
			case 6:
				deli.newDay();
				break;
			case 7: 
				cout<<"Thanks for coming! Have a great day :)"<< endl; 
				break;
			default:
				choice = 0;  
				cout<<"Invalid choice!"<< endl; 
		}
		cout<<endl; 
	}while(choice != 7);
	return 0; 
}
