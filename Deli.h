//Deli Class 

#ifndef DELI_H
#define DELI_H

#include "Cart.h"
#include "Price_list.h"
#include "Scale.h"

using std::string; 

#define DSIZE 10
#define DS 7

class Cart;

class Deli
{

public: 
	Deli(); //default (10 for the max # of customers)
	~Deli(); 
	void setCarts(); //Will make each element in the array of pointers to carts NULL 
	void changePrices(); //will call setPrices in Price_list  
	void newCustomer(); // create a new customer with an ID #
	Cart* selectCustomer(); // select a customer to serve
	void checkOut(Cart *c); //check out customer  
	void report(); //print reveues for the day 
	void newDay(); //start a new day 
private:
 	//Array of "Cart" to hold pointers to each customer's cart
 	Cart *Customers[DSIZE];
	//Array of IDs that correlate the the ID of the Customer 
	int CustIDs[DSIZE]; 
	//Holds the prices for this deli
	Price_list prices; 
	//Scale for the deli 
	Scale scale;  
	static int countID; //will be used to assign individual IDs

	//Will be used to keep track of the totals 	
	float totalWeight[DS];
	float totalRevPro[DS]; 
	float totalRevenue; 

};

#endif 	
