//Price_list.cpp

#include <iostream>
#include <string>
#include "Price_list.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

//Constructor
//will fill products array and prices array with the names and default prices of
//the items in the cart respectively
Price_list::Price_list()
{
	products[0] = "";
	products[1] = "Roast Beef"; 
	products[2] = "Pastrami"; 
	products[3] = "Salami";
	products[4] = "Pickles"; 
	products[5] = "Black Olives"; 
	products[6] = "Green Olives"; 

	prices[0] = 0;
	prices[1] = 5;
	prices[2] = 6;
	prices[3] = 5;
	prices[4] = 4;
	prices[5] = 3;
	prices[6] = 2;
}

//This function will allow a customer to see the current prices and then choose
//a price to reset 
void Price_list::setPrice()
{
	int index;
	float price;
	printPrice();
	cout << "For which item would you like to change the price?" << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "1. Roast Beef" << endl;	
	cout << "2. Pastrami" << endl;
	cout << "3. Salami" << endl;
	cout << "4. Pickles" << endl;
	cout << "5. Black Olives" << endl;
	cout << "6. Green Olives" << endl; 
	cout << "Please enter choice: ";
	cin >> index;
	cout << "\nThe current price of " << products[index] << " is: $" << prices[index] << " per lbs" << endl; 
	cout << "What would you like to change it to? $"; 
	cin >> price;   	
	prices[index] = price; 
	cout << "\nThe price of " << products[index] << "was changed to: $" << prices[index] << " per lbs"<< endl;
}

//This will return the price at a given index
float Price_list::getPrice( int index ) const
{
	return prices[index]; 
}

//This will return the product name at a given index
string Price_list::getProduct( int index ) const
{
	return products[index];
}

//This will print the list of prices 
void Price_list::printPrice() 
{
	cout << "Price List: " << endl;
	cout << "------------" << endl; 
	int i; 
	for( i = 1; i <= 6; i++ ) 	
	{
		cout << products[i] << ".....$" << prices[i] << endl; 
	}
	cout<<endl;
}
