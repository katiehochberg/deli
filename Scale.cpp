//Scale
#include <iostream>
#include <string>
#include "Scale.h"

using std::string;
using std::cin; 
using std::cout; 
using std::endl;

//Default constructor
Scale::Scale() 
{
	setWeight(1.0);
}	

//Constructor when passed a given weight
Scale::Scale( float weight )
{
	setWeight(weight); 
}

//Menu 
//will allow a user to determine the exact weight they want by using a 
//do while loop to allow them to add, subtract, or zero until they 
//decide to add that item to their cart
float Scale::menu( string name ) 
{
	int choice;
	float op; 
	do {
		cout << "----------------------------------------" <<endl;
		cout << "You currently have: " << weight << " lbs of " << name << endl;
		cout << "----------------------------------------" <<endl;
		cout <<endl;
		cout << "Would you like to: " << endl;
		cout << "------------------" << endl;
		cout << "1. Add weight" << endl;
		cout << "2. Subtract weight" << endl; 
		cout << "3. Zero weight" << endl; 	
		cout << "4. Add to cart" << endl;
		cout << "Please enter choice: "; 
		cin >> choice; 
		cout << endl;	
		switch (choice)
		{
			case 1: 
				cout << "How much weight would you like to add? "; 
				cin >> op; 
				add( op ); 	
				break; 
			case 2: 	
				cout << "How much weight would you like to subtract? "; 
				cin >> op; 
				sub( op ); 
				break; 
			case 3: 
				cout << "You chose to set the weight to zero" << endl; 
				zero(); 
				break; 
			case 4: 
				break; 
			default:
				cout << "Invalid choice!" << endl; 
				break;	
		}
		cout<<endl;
	} while ( choice != 4 );
	return weight; 	
}	

//Setter function to set weight
void Scale::setWeight( float weight )
{
	this->weight = weight; 
}

//Will add a given amount to the weight
void Scale::add( float weight )
{
	this->weight = this->weight + weight; 
}

//Will subtract a given amount from the weight
void Scale::sub( float weight ) 
{
	this->weight = this->weight - weight; 
	if( this->weight < 0 ) this->weight = 0; 
}

//Will set the weight to zero
void Scale::zero() 
{
	weight = 0; 
}

//Return weight
float Scale::getWeight() const
{
	return weight; 
}
