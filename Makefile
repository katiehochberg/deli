C++ = g++
CFLAGS = -c -g 

all: deliDriver

deliDriver: deliDriver.o Deli.o Cart.o Scale.o Price_list.o 
		$(C++) -o deliDriver deliDriver.o Deli.o Cart.o Scale.o Price_list.o

clean:
		rm -f *.o 

%.o:	%.cpp
		$(C++) $(CFLAGS) $*.cpp
