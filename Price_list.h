//Price_list Class

#ifndef PRICE_H
#define PRICE_H

#include <iostream>
#include <string>

using std::string;

#define PSIZE 7

class Price_list
{
public:
	Price_list(); //Constructor
	void setPrice(); //setter function
	float getPrice( int ) const; //return price of given index
	string getProduct( int ) const; //return product name of given index
	void printPrice(); //will print the list of prices
private: 
	float prices[PSIZE]; //holds prices of each product
	string products[PSIZE]; //holds names of each product
};

#endif
	
