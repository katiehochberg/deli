//Cart.cpp

#include <iostream>
#include <string>
#include "Cart.h"
#include "Price_list.h"
#include "Scale.h" 
#include "Deli.h"

using std::cout;
using std::cin; 
using std::endl; 
using std::string;

CartList::CartList( const data &data )
	: content( data ), next( NULL )  
{
	//Empty body
}

data CartList::getCartList() const
{
	return content; 
}

//Cart constructor 
//Sets the first and current of the linked list to NULL
Cart::Cart()
{
	first = NULL; 
	current = NULL; 
} 

//Cart deconstructor 
//Will delete each node of the linked list
Cart::~Cart()
{
	CartList *ptr1;
	while( first != NULL ) 
	{
		ptr1 = first; 
		first = ptr1->next; 
		delete ptr1;
		cout << "Destructor: removing a node" <<endl;
	}
}

//Will allow a customer to select a product 
//The customer can than go to the scale class and determine how much they want
//Then the customer can add it to their cart
//	The function will create a new node for that item and call addToCart
void Cart::selectProduct( Scale scale, Price_list prices ) 
{ 
	int choice; 
	int index;
	do {
		scale.zero(); 
	 	cout<< "\nPlease select by entering the number of your choice: " << endl; 
		cout<< "----------------------------------------------------" << endl;
		cout<< "1. Roast Beef...$" << prices.getPrice(1) << " per lbs" << endl; 
		cout<< "2. Pastramii...$" << prices.getPrice(2) << " per lbs" << endl; 
		cout<< "3. Salami...$" << prices.getPrice(3) << " per lbs" << endl; 
		cout<< "4. Pickles...$" << prices.getPrice(4) << " per lbs" << endl; 
		cout<< "5. Black Olives...$"<< prices.getPrice(5) << " per lbs" << endl; 
		cout<< "6. Green Olivesi...$"<< prices.getPrice(6) << " per lbs" << endl;
		cout<< "Please enter choice: "; 
		cin>> index;
		if( index < 1 || index > 6 ) 
		{
			cout<< "Invalid choice!"<< endl;
			choice = 2;
		}
		else
		{
			name = prices.getProduct(index);
			price = prices.getPrice(index); 	 
			weight = scale.menu(name); 
			type = index;
			cout<< "You have chosen " << weight << " lbs of " << name << endl;
			cout<< "Is this correct (enter 1 for yes, 2 to select a new product): ";
			cin>> choice; 
			if (choice == 1) 
			{ 
				data d; 
				d.name = name; 
				d.weight = weight; 
				d.type = type;
				d.price = weight*price;  
				addToCart(d);  
			}
			else if ( choice != 1 || choice != 2 ) 
				cout << "Invalid choice!" << endl;
		}
	} while ( choice != 1 );	
}

//Will add a data to the list
//Similar to an insert for a linked list
void Cart::addToCart(const data &d)
{
	CartList *a = new CartList(d); 
	if( first == NULL )
	{
		first = a;
		current = a;
	} 
	else 
	{
		current->next = a; 
		current = current->next;
	}
	cout << d.weight << " lbs of " << d.name << " was added to your cart" << endl; 
}

//Will check out the customer and return a pointer to an array 
//that holds the total weight of each product that the Cart
//is going to buy
float* Cart::checkOut()
{
	float *w; 
	CartList *ptr1;
	ptr1 = first; 
	data g; 
	while( ptr1 != NULL )
	{
		g = ptr1->getCartList(); 
		totalProduct[g.type] += g.weight;  
		ptr1 = ptr1->next;
	}
	w = totalProduct; 
	return w;
}

//Will print the current invoice
void Cart::print()
{
	cout<< "\nInvoice:" << endl; 
	cout<< "--------" << endl; 
	CartList *ptr2;
	data k;
	ptr2 = first; 
	while( ptr2 != NULL ) 
	{
		k = ptr2->getCartList(); 
		cout<< k.name << "..." << k.weight << " lbs...$" << k.price << endl;
		ptr2 = ptr2->next;
	}
	cout<< "Total.........$" << getPrice() << endl; 	
}

//Will return the total price currently in the cart by adding the 
//price for each element 
float Cart::getPrice()
{
	float p = 0;
	CartList *ptr3; 	
	data m; 
	ptr3 = first; 
	while( ptr3 != NULL ) 
	{
		m = ptr3->getCartList(); 
		p += m.price;
		ptr3 = ptr3->next;  
	}
	return p; 
}

