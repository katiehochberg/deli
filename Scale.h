//Scale Class
//Functions:
//	Add weight
//	Subtract weight
//	Zero weight 
//	Get weight

#ifndef SCALE_H
#define SCALE_H

#include <string>

using std::string;

class Scale
{
public:
	Scale(); //default constructor
	Scale( float );
	float menu( string );  
	void add( float ); //will add given weight to product 
	void sub( float ); //will subtract given weight from product
	void zero(); //will set weight to zero
	void setWeight( float ); //will set weight
	float getWeight() const; //will return weight
private:
	float weight;  
};

#endif
