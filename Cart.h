//Cart Class

#ifndef CART_H
#define CART_H 

#include <string>
#include "Deli.h"
#include "Scale.h"
#include "Price_list.h"

using std::string;

#define CSIZE 7

typedef struct dummy_node{
	string name; 
	float weight; 
	float price; 
	int type; 
} data; 

class CartList
{
	friend class Cart;
public:
	CartList( const data &data ); 
	data getCartList() const;
private:
	data content; 
	CartList *next;
};

class Cart
{
public:
	Cart();
	~Cart();    
	void selectProduct( Scale, Price_list ); 
	void addToCart(const data&); 
	void print();
	float* checkOut(); 	
	float getPrice(); 
private: 
	//Total Product Sold to that Customer
	float totalProduct[CSIZE]; 		

	//Information for new Item
	string name;
	float weight;
	float price; 
	int type; 	
	
	//First and Current pointers to list
	CartList *first; 
	CartList *current;
};

#endif 
