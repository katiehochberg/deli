//Deli 

#include <iostream>
#include "Deli.h"
#include "Cart.h"
#include "Scale.h"
#include "Price_list.h"

using std::cout;
using std::cin;
using std::endl; 
using std::string; 

int Deli::countID = 1;  
Cart *current = NULL; 

//Constructor
Deli::Deli()
{
	setCarts(); 
	Price_list prices;
	int i; 
	for( i = 0; i < DS; i++ ) 
	{
		totalWeight[i] = 0; 
		totalRevPro[i] = 0; 
	} 
	totalRevenue = 0;
}

//Deconstructor 
Deli::~Deli()
{
	int i; 
	for( i = 0; i < DSIZE; i++ )
	{
		if( Customers[i] != NULL )
		{
			Customers[i]->~Cart(); 
		}
	} 	 	
	cout<<"Deli deconstructed!"<< endl; 
}

//Will set each element of the array of pointers to carts to NULL
void Deli::setCarts()
{
	int i; 
	for( i = 0; i < DSIZE; i++ )
	{
		Customers[i] = NULL; 
		CustIDs[i] = 0; 
	}
}

//Will call the setPrice function for price_list so user can change 
//the prices of the items in the deli
void Deli::changePrices() 
{
	prices.setPrice(); 
}

//This will createa a new customer 
void Deli::newCustomer()
{	
	Cart *customer = new Cart; 
	int i; 
	int index = -1;
	for( i = 0; i < DSIZE; i++ )
	{
		if( Customers[i] == NULL )
		{
		 	index = i;  
			i = DSIZE + 1; 
		}
	} 
	if( index == -1 )
	{
		cout<< "There are too many customers in the deli!" << endl; 
		cout<< "You will need to check out a customer before another can enter!" << endl; 
	}
	else
	{
		Customers[index] = customer; 
		CustIDs[index] = countID;
		cout<<"\n---------------------------------"<<endl; 
		cout<< "Customer " << countID << " has entered the deli!" << endl;
		cout<< "---------------------------------" <<endl; 
		countID++; 
	}
}

//Select a customer and return a pointer to that customer
Cart* Deli::selectCustomer() 
{
	int i; 
	int choice;
	int sum = 0;
	for( i = 0; i < DSIZE; i ++ ) 
	{	
		sum += CustIDs[i];
	} 
	if (sum == 0) 
	{
		cout<< "There are no customers in the deli!" << endl; 
	}
	else
	{
		cout << "\nHere are the customers currently in the Deli:" << endl; 
		cout << "---------------------------------------------" << endl;
		for( i = 1; i <= DSIZE; i ++ ) 
		{
			if( Customers[i - 1] == NULL ) 
				cout << i << ". Empty Customer Slot" << endl;
			else 
				cout << i << ". Customer " << CustIDs[i-1] << endl; 	
		}	
		cout << "Please enter the number that correlates with your choice: "; 	
		cin >> choice;
		current = Customers[choice-1];
		while ( CustIDs[choice-1] == 0 ) 
		{
			cout<< "\nThere is no customer in this slot, please select a different customer" << endl;
			cout<< "Choice: "; 
			cin >> choice; 
		}  	 
		cout<< "\nYou have selected to serve Customer " << CustIDs[choice-1] << endl;
		cout<< "------------------------------------" << endl; 
		do 	
		{
			cout<< "Would you like to: " << endl;
			cout<< "------------------" << endl; 
			cout<< "1. Select a product" << endl; 
			cout<< "2. Print current cart contents" << endl; 
			cout<< "3. Check out " << endl; 
			cout<< "4. Have a minute" << endl; 
			cout<< "Please enter your choice: "; 
			cin>> choice; 
			switch (choice)
			{
				case 1: 
					current->selectProduct(scale, prices); 
					break; 
				case 2:	
					current->print(); 
					break; 
				case 3:
					checkOut(current);
					break; 
				case 4:
					break; 
				default: 
					cout<< "Invalid choice!" <<endl; 
					choice = 1; 
					break;
			}
			cout<<endl;	
		} while (choice == 1 || choice == 2); 
		return current;  
	}
	return NULL; 
}

//Will call the checkOut function for a Cart 
//Will add up the total revenues 
void Deli::checkOut(Cart *c)
{
	float *w;
	w = c->checkOut(); 
	int i; 
	int id; 
	for( i = 1; i < DS; i++ ) 
	{
		totalWeight[i] += w[i];
		totalRevPro[i] += w[i] * prices.getPrice(i); 
	} 
	totalRevenue += c->getPrice();
	for( i = 0; i < DSIZE; i++ )
	{
		if( c == Customers[i] ) 
		{
			Customers[i] = NULL; 
			id = CustIDs[i];
			CustIDs[i] = 0; 
		}
	}
	c->print();
	c->~Cart();
	cout<< "Customer " << id << " has been checked out!" << endl; 	
}

//Prints out all the revenues for each product and the total for the day
void Deli::report() 
{
	cout<< "\nRevenues Per Product: " << endl; 
	cout<< "----------------------" << endl;
 	int i; 
	for( i = 1; i < DS; i++ ) 
	{
		cout<< prices.getProduct(i) << " " << totalWeight[i] << " lbs $" << totalRevPro[i] << endl;
	}		
	cout << "Total revenue: $" << totalRevenue << endl; 	
	cout << endl;
}

//Will reset revenues for the day
void Deli::newDay()
{
	int i;
	for( i = 0; i < DSIZE; i++ )
	{
		if( Customers[i] != NULL )
		{
			checkOut(Customers[i]);  
			Customers[i] = NULL; 
		}
	}
	report(); 
	for( i = 0; i < DS; i ++ )
	{
		totalWeight[i] = 0;
		totalRevPro[i] = 0;	
	} 
	totalRevenue = 0;
	countID = 1; 
	cout << "See you tomorrow!" << endl; 
}





